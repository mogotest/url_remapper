require 'rubber'

ENV['RUBBER_ROOT'] ||= File.join(File.dirname(__FILE__), '..', 'mogotest/')
ENV['RUBBER_ENV'] ||= 'development'

Rubber.initialize(ENV['RUBBER_ROOT'], ENV['RUBBER_ENV'])

require './url_remapper.rb'
run UrlRemapper
