require_relative 'test_helper'

class UrlRemapperTest < MiniTest::Unit::TestCase
  include Rack::Test::Methods

  def app
    UrlRemapper
  end

  def setup
    MogoRedis.redis { |redis| redis.flushdb }
  end

  def test_when_remapping_data_is_present
    MogoRedis.redis { |redis| redis.hset('remapped_urls', 'www.example.com', '3kac.kevin.mogotunnel.com') }

    get "/?host=www.example.com&scheme=http&port=80&secret_token=#{Rubber.config['trafficserver_remap_api_secret_token']}"

    assert_equal 200, last_response.status
    assert_equal '3kac.kevin.mogotunnel.com:80:http', last_response.body
  end

  def test_when_remapping_data_does_not_exist
    get "/?host=www.example.com&scheme=http&port=80&secret_token=#{Rubber.config['trafficserver_remap_api_secret_token']}"

    assert_equal 200, last_response.status
    assert_equal 'www.example.com:80:http', last_response.body
  end

  def test_when_secret_token_does_not_match
    get "/?host=www.example.com&scheme=http&port=80&secret_token=bad_token"

    assert_equal 403, last_response.status
  end
end
