ENV['RACK_ENV'] = 'test'

require 'bundler'
Bundler.require(:default, ENV['RACK_ENV'].to_sym)

# enable coverage reports for jenkins only
if ENV['CI']
  puts 'Enabling simplecov(rcov) for Jenkins'
  require 'simplecov'
  require 'simplecov-rcov'
  SimpleCov.formatter = SimpleCov::Formatter::RcovFormatter
  SimpleCov.start
end

ENV['RUBBER_ROOT'] ||= File.join(File.dirname(__FILE__), 'fixtures')
ENV['RUBBER_ENV'] ||= 'test'
Rubber.initialize(ENV['RUBBER_ROOT'], ENV['RUBBER_ENV'])

require_relative '../url_remapper'

require 'minitest/autorun'
require 'rack/test'
require 'minitest/reporters'
require 'minitest/mock'

MiniTest::Reporters.use! unless ENV['CI']
