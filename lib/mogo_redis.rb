require 'redis'
require 'connection_pool'

class MogoRedis
  POOL = ConnectionPool.new(:size => 25, :timeout => 5) { Redis.new(:host => (Rubber.instances.for_role('redis_master').first.name rescue 'localhost'), :port => Rubber.config['redis_server_port'], :db => Rubber.config['redis_mogo_db']) }

  def self.redis(&block)
    raise ArgumentError, "requires a block: #{caller.inspect}" if !block

    POOL.with(&block)
  end
end