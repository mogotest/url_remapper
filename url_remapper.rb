require 'sinatra/base'
require 'rubber'
require 'readline'
require 'torquebox/logger'

require_relative 'lib/mogo_redis'

module TorqueBox
  module Middleware
    class Logger
      def initialize(app, logger = nil)
        @app = app
        @logger = logger
      end

      def call(env)
        env['rack.logger'] = logger

        @app.call(env)
      end

      private

      def logger
        @logger ||= TorqueBox::Logger.new(@app.class)
      end
    end
  end
end

# For looking up URL remap rules based upon SSH tunneled connections for the proxy server.
class UrlRemapper < Sinatra::Base
  use TorqueBox::Middleware::Logger

  configure :production, :development do
    enable :logging, :dump_errors
  end

  get '/' do
    if params[:secret_token] == Rubber.config['trafficserver_remap_api_secret_token']
      host = params[:host].split(':').first
      remapped_host = MogoRedis.redis { |redis| redis.hget('remapped_urls', host) }

      if remapped_host
        "#{remapped_host}:80:http"
      else
        "#{host}:#{params[:port]}:#{params[:scheme]}"
      end
    else
      403
    end
  end
end
