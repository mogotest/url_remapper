# Introduction #

This is a very simple Sinatra application used for looking up URL rewrite rules.  This was used in the defunct Mogotest
service for routing a customer's tests over a private tunnel for behind the firewall access.  On its own it's not
terribly useful, but it is a necessary component of the larger Mogotest suite.

The code is licensed under the Apache License, version 2.0. This project is no longer maintained but is reasonably
up-to-date Ruby.

# Running #

As written, this app expects to be running within the [TorqueBox](http://torquebox.org/) application server, implying
a dependency on JRuby.  It also expects to be deployed with a pre-existing [Rubber](http://rubber.io/) configuration.
Divorcing from any of these constraints would be a straightforward exercise.  The project is currently in maintenance
mode, but a pull request doing either would be welcomed.

The key part is there is a shared secret key between this application and the Mogotest Apache Traffic Server remapper
plugin.  This application looks up the shared token and verifies that it's correct on every request.  While not the
strongest authentication mechanism in the world, the application has no need to be publicly exposed since it's only
for internal service API use.

# In Detail #

In Mogotest, customers could run a [client](https://github.com/mogotest/localtunnel-jvm-client) providing a reverse
SSH connection from a local server to Mogotest.  This allowed Mogotest to access a private site over a randomly
generated hostname.  When this tunnel is established, a record is made within Mogotest in the form of a URL mapping.

Since tunnels can come and go and they're randomly generated, it's far easier for a test configuration to continue
using a canonical hostname end-to-end.  However, in order for any of the browsers being tested in to know to use the
tunnel, a transparent remapping must occur.  In the Mogotest architecture, all browsers route their requests through
a caching proxy server (Apache Traffic Server) in order to speed up tests, not DoS a site, and so all requests originate
from a set of known IP addresses.  The remapping occurs at this layer.

The Mogotest Apache Traffic Server (ATS) URL rewriting plugin looks up every request to see if it should be rewritten.
This Sinatra app is the means by which the ATS plugin performs that lookup.  By having the plugin perform a simple
HTTP request we were able to reduce the dependencies of the plugin (e.g., it doesn't need to bind directly to Redis)
and it allowed us to keep most of the complex logic in Ruby, where it's easily changeable.